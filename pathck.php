<?php

require "composer\src\Composer\Util\ProcessExecutor.php";
require "composer\src\Composer\Util\Filesystem.php";
require "composer\src\Composer\Util\Platform.php";

function ckmodes ($path) {
    echo 'checking ' . $path . "\n";
    clearstatcache(true, $path); // safety
    clearstatcache(false); // safety
    $stat_full = stat($path);
    clearstatcache(true, $path); // absolutely necessary
    clearstatcache(false); // absolutely necessary
    $lstat_full = lstat($path);
    $realpath = realpath($path);

    echo 'realpath  ' . var_export($realpath, true) . "\n";
    echo 'paths match ' . (($path === $realpath) ? "yes" : "no") . "\n";
    echo 'stat nlink  ' . var_export(stat($path)['nlink'], true) . "\n";
    echo 'stat mode ' . var_export(stat($path)['mode'], true) . "\n";
    echo 'lstat mode ' . var_export(lstat($path)['mode'], true) . "\n\n";
    echo 'array_diff ' . var_export(array_diff_assoc($stat_full, $lstat_full), true) . "\n\n";;
    echo 'is_dir ' . (is_dir($path) ? 'yes' : 'no')
        . ', is_link ' . (is_link($path) ? 'yes' : 'no')
        . ', mode & 0xC000 ' . ($lstat_full['mode'] & 0xC000) . "\n\n";
}

function stat_test_junction($path)
{
    clearstatcache(true, $path); // safety
    $stat_mode = stat($path)['mode'];
    clearstatcache(true, $path); // absolutely necessary
    $lstat_mode = lstat($path)['mode'];

    return strcmp($stat_mode, $lstat_mode) !== 0;
}

function zero_lstat_test_junction ($path) {
    clearstatcache(true, $path); // absolutely necessary
    $lstat_mode = lstat($path)['mode'];

    return !($lstat_mode & 0xC000) /*|| ($lstat_mode === 0)*/;
}

function test_junction($path) {
    echo $path . (zero_lstat_test_junction($path) ? " is " : " is not ") . "a junction\n";
}

function isWindows()
{
    return defined('PHP_WINDOWS_VERSION_BUILD');
}

function isJunction($junction)
{
    if (! /* Platform:: */ isWindows()) {
        return false;
    }
    if (!is_dir($junction) || is_link($junction)) {
        return false;
    }
    /**
     * According to MSDN at https://msdn.microsoft.com/en-us/library/14h5k7ff.aspx we can detect a junction now
     * using the 'mode' value from stat: "The _S_IFDIR bit is set if path specifies a directory; the _S_IFREG bit
     * is set if path specifies an ordinary file or a device." We have just tested for a directory above, so if
     * we have a directory that isn't one according to lstat(...) we must have a junction.
     *
     * #define	_S_IFDIR	0x4000
     * #define	_S_IFREG	0x8000
     *
     * Stat cache should be cleared before to avoid accidentally reading wrong information from previous installs.
     */
    clearstatcache(true, $junction);
    $stat = lstat($junction);

    return !($stat['mode'] & 0xC000);
}

function orig_test_junction($path) {
    // echo $path . (isJunction($path) ? " is " : " is not ") . "a junction\n";

    $filesystem = new \Composer\Util\Filesystem();

    echo $path . ($filesystem->isJunction($path) ? " is " : " is not ") . "a junction\n";

}

function orig_test_symlink($path) {

    $filesystem = new \Composer\Util\Filesystem();

    echo $path . ($filesystem->isSymlinkedDirectory($path) ? " is " : " is not ") . "a symlinked directory\n";
}

$junctiondir = 'C:\vagrant\gnu\compoveri\vendor/narra/a-repo';
$normdir = 'C:\vagrant\gnu\compoveri\vendor';
$junctiondir_win = 'C:\vagrant\gnu\compoveri\vendor\narra\a-repo';
$junctiondir_nix = 'C:/vagrant/gnu/compoveri/vendor/narra/a-repo';
$junctionedfile = 'C:\vagrant\gnu\compoveri\vendor/narra/a-repo/composer.json';

echo "\nfirst, folders: \n\n";
ckmodes($junctiondir);
ckmodes($normdir);
ckmodes($junctiondir_win);
ckmodes($junctiondir_nix);
ckmodes('.');  // local directory

echo "now, files:\n\n";
ckmodes($junctionedfile);
ckmodes($argv[0]);  // self

echo "finally, truths:\n\n";
test_junction($junctiondir);
test_junction('.');
test_junction($junctionedfile);
test_junction($argv[0]);

echo "\nbut which ones, truths; let's see the original iJunction's result:\n\n";
orig_test_junction($junctiondir);
orig_test_junction('.');
orig_test_junction($junctionedfile);
orig_test_junction($argv[0]);

echo "\nand also, let's see the original isSymlinkedDirectory's result:\n\n";
orig_test_symlink($junctiondir);
orig_test_symlink('.');
orig_test_symlink($junctionedfile);
orig_test_symlink($argv[0]);
